from datetime import datetime
str_d1=input("Enter Date 1 in YYYY,MM,DD format\n")
str_d2=input("Enter Date 2 in YYYY,MM,DD format\n")
d1,d2=None,None
try:
    d1 = datetime.strptime(str_d1, "%Y,%m,%d")
    d2 = datetime.strptime(str_d2, "%Y,%m,%d")
except ValueError:
    print("Enter Date in YYYY,MM,DD format only, for example: 2023,03,21")
    exit()
difference=(d2-d1).days
if difference==1:
    print("There is 1 Day Between {0} and {1}".format(str_d1,str_d2))
else:print("There are {2} Days Between {0} and {1}".format(str_d1,str_d2,difference))